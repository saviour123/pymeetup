# GITLAB Overview and CI Ops

This sample shows how to use [Flask](http://flask.pocoo.org/) to handle
requests, forms, templates, and static files on Google App Engine Standard.

Before running or deploying this application, install the dependencies using
[pip](http://pip.readthedocs.io/en/stable/):

## Setup Virtualenv and Install dependencies

    virtualenv env && pip install -r requirements.txt

## Test

    pytest main_test.py

## License

MIT
